ChatBot = {}

function ChatBot.OnEvent(wtfisthis, event, arg1, arg2)
    if event == "ADDON_LOADED" and arg1 == "ChatBot" then
        SLASH_ChatBot1, SLASH_ChatBot2 = "/ChatBot", "/CB"
        SlashCmdList["ChatBot"] = ChatBot.ChatCommandHandler
        ChatBot.SavedVars()
    -- other AddOns might load before and throw errors if this is not initialized yet
    elseif ChatBotCFG and ChatBotCFG["on"] == 1 then
        if event == "CHAT_MSG_WHISPER" then
            ChatBot.HandleReply(arg1,arg2)
        end
    end
end

function ChatBot.SavedVars()
    if not ChatBotCFG then
        ChatBotCFG = {}
        ChatBotCFG["on"] = 1
    end
    if not ChatBotSV then
        ChatBotSV = {}
        ChatBotSV["GLOBAL"] = {}
        ChatBotSV["GLOBAL"][1] = {}
        ChatBotSV["GLOBAL"][1].msg = {"ahoj"}
        ChatBotSV["GLOBAL"][1].reply = {}
        ChatBotSV["GLOBAL"][1].reply[1] = {}
        ChatBotSV["GLOBAL"][1].reply[1].msg = "ahoj"
        ChatBotSV["GLOBAL"][1].reply[1].priority = 1
    end
end

function ChatBot.GetCommand(msg)
    if msg then
        local a,b,c=strfind(msg, "(%S+)")
        if a then
            return c, strsub(msg, b+2)
        else
            return ""
        end
    end
end

function ChatBot.GetString(msg)
    if msg then
        local ret1,ret2
        local a,b,c=strfind(msg, '("+..+")')
        if a then 
            -- check for multiple strings
            d = strfind(msg,'"',a+1,b-1)
            ret1 = strsub(msg,a+1,d-1)
            ret2 = strsub(msg,d+1)
        else
            ret2 = ""
        end
        return ret1, ret2
    end
end

--/cb add GLOBAL 1 msg ahoj jak je
function ChatBot.Add(message)
    local name, rest, index, cmd, msg, prio
    name, rest = ChatBot.GetCommand(message)
    name = string.upper(name)
    if not ChatBotSV[name] then
        return ChatBot.Print("Nelze pridavat nastaveni pro neexistujici nick")
    end
    index, rest = ChatBot.GetCommand(rest)
    if not index then
        return ChatBot.Print("Neplatny index")
    end
    index = string.upper(index)
    if index ~= "DEFAULT" then
        index = tonumber(index)
        if not index then
            return ChatBot.Print("Neplatny index")
        end
    else
        if not ChatBotSV[name][index] then
            ChatBotSV[name][index] = {}
            ChatBotSV[name][index].reply = {}
        end
    end
    if not ChatBotSV[name][index] then
        return ChatBot.Print("Index "..index.." pro nick "..name.." neexistuje")
    end
    if index ~= "DEFAULT" then
        msg, rest = ChatBot.GetString(rest)
        local i = 1
        if msg and msg ~= "" then
            while ChatBotSV[name][index].msg[i] do
                if string.lower(ChatBotSV[name][index].msg[i]) == msg then
                    i = 0
                    break
                end
                i=i+1
            end
            if i ~= 0 then
                ChatBotSV[name][index].msg[i] = msg
            end
        end
    end
    reply, prio = ChatBot.GetString(rest)
    if not reply or reply == "" then
        return
    end
    local i = 1
    while ChatBotSV[name][index].reply[i] do
        if ChatBotSV[name][index].reply[i].msg == reply then
            i = 0
            break
        end
        i=i+1
    end
    if i ~= 0 then
        ChatBotSV[name][index].reply[i] = {}
        ChatBotSV[name][index].reply[i].msg = reply
        prio = tonumber(prio)
        if not prio then
            prio = 1
        end
        ChatBotSV[name][index].reply[i].priority = prio
    end
end
--/cb create GLOBAL "Ahoj" "Cs" 1
function ChatBot.Sort(table, start)
    while table[start+1] or table[start] do
        table[start] = table[start+1]
        start=start+1
    end
end

function ChatBot.Create(message)
    local name, rest, index, cmd, msg, reply, prio
    name, rest = ChatBot.GetCommand(message)
    name = string.upper(name)
    if not ChatBotSV[name] then
        ChatBotSV[name] = {}
        index = 1
    else
        local i = 1
        while ChatBotSV[name][i] do
            i=i+1
        end
        index = i
    end
    msg, rest = ChatBot.GetString(rest)
    reply, prio = ChatBot.GetString(rest)
    if msg and msg ~= "" then
        ChatBotSV[name][index] = {}
        ChatBotSV[name][index].msg = {msg}
        ChatBotSV[name][index].reply = {}
    end
    if not reply or reply == "" then
        return
    end    
    ChatBotSV[name][index].reply[1] = {}
    ChatBotSV[name][index].reply[1].msg = reply
    prio = tonumber(prio)
    if not prio then
        prio = 1
    end
    ChatBotSV[name][index].reply[1].priority = prio
end

function ChatBot.Del(message)
    local name, rest, index, cmd, msg, reply, prio
    name, rest = ChatBot.GetCommand(message)
    name = string.upper(name)
    if not ChatBotSV[name] then
        return ChatBot.Print("Nelze smazat neexistujici nick")
    end
    index, rest = ChatBot.GetCommand(rest)
    if not index or index == "" then
        ChatBotSV[name] = nil
        return
    end
    index = string.upper(index)
    if index ~= "DEFAULT" then
        index = tonumber(index)
        if not index then
            return ChatBot.Print("Neplatny index")
        end
    end
    if not rest or rest == "" then
        ChatBotSV[name][index] = nil
        ChatBot.Sort(ChatBotSV[name],index)
        return
    end    
    if not ChatBotSV[name][index] then
        return ChatBot.Print("Index "..index.." pro nick "..name.." neexistuje")
    end
    if index ~= "DEFAULT" then    
        msg, rest = ChatBot.GetString(rest)
        local i = 1
        if msg and msg ~= "" then
            while ChatBotSV[name][index].msg[i] do
                if string.lower(ChatBotSV[name][index].msg[i]) == string.lower(msg) then
                    ChatBotSV[name][index].msg[i] = nil
                    ChatBot.Sort(ChatBotSV[name][index].msg, i)
                    break
                end
                i=i+1
            end
        end
    end        
    reply, prio = ChatBot.GetString(rest)
    if reply and reply ~= "" then
        local i = 1
        while ChatBotSV[name][index].reply[i] do
            if string.lower(ChatBotSV[name][index].reply[i].msg) == string.lower(reply) then
                ChatBotSV[name][index].reply[i] = nil
                ChatBot.Sort(ChatBotSV[name][index].reply, i)
                break
            end
            i=i+1
        end
    end
end

function ChatBot.Print(msg,offset)
    if not msg then
        return
    end
    local spaces = ""
    if offset then 
        while offset ~= 0 do
            offset=offset-1
            spaces=spaces.."| "
        end
    end
    print("|cff601020"..spaces..msg)
end

function ChatBot.Show()
    local name, table
    for name, table in pairs(ChatBotSV) do
        ChatBot.Print(name..":",0)
        local i = 1
        while table[i] do
            ChatBot.Print("index "..i,1)
            ChatBot.Print("msg:",2)
            local j = 1
            while table[i].msg[j] do
                ChatBot.Print(table[i].msg[j],3)
                j=j+1
            end
            j = 1
            ChatBot.Print("reply:",2)
            while table[i].reply[j] do
                ChatBot.Print("Prio "..table[i].reply[j].priority.." reply:",3)
                ChatBot.Print(table[i].reply[j].msg,4)
                j=j+1
            end
            i=i+1
        end
        if table["DEFAULT"] then
            ChatBot.Print("DEFAULT: "..i,1)
            local j = 1
            ChatBot.Print("reply:",2)
            while table["DEFAULT"].reply[j] do
                ChatBot.Print("Prio "..table["DEFAULT"].reply[j].priority.." reply:",3)
                ChatBot.Print(table["DEFAULT"].reply[j].msg,4)
                j=j+1
            end
            i=i+1        
        end
    end
end

function ChatBot.ChatCommandHandler(msg)
    local cmd,cmd2 = ChatBot.GetCommand(msg)
    if cmd then cmd = string.lower(cmd) end
    if cmd == "add" then
        ChatBot.Add(cmd2)
    elseif cmd == "create" then
        ChatBot.Create(cmd2)
    elseif cmd == "del" then
        ChatBot.Del(cmd2)
    elseif cmd == "show" then
        ChatBot.Show()
    end
end

function ChatBot.HandleReply(message, plr_name)
    local msg  = string.lower(message)
    local name = string.upper(plr_name)
    local reply = nil
    if ChatBotSV[name] then
        reply = ChatBot.GetReply(true ,msg, name)
    end
    if not reply then
        reply = ChatBot.GetReply(false ,msg, name)
    end
    if reply then
        SendChatMessage(reply,"WHISPER",nil,name)
    end
end

function ChatBot.WasRecentlyUsed(specific,index, name)
    if not ChatBot[name] then
        return nil
    end
    if not ChatBot[name][specific] then
        return nil
    end
    local i
    for i=1,5 do
        if ChatBot[name][specific][i] == index then
            return true
        end
    end
    return nil
end

function ChatBot.AddRecentlyUsed(specific,index,name)
    if not ChatBot[name] then
        ChatBot[name] = {}
    end
    if not ChatBot[name][specific] then
        ChatBot[name][specific] = {}
    end
    local i
    for i = 1,5 do
        ChatBot[name][specific][i+1] = ChatBot[name][specific][i]
    end
    ChatBot[name][specific][1] = index
end

function ChatBot.GetReply(specific, message, name)
    local list
    if specific == true then
        list = ChatBotSV[name]
    else
        list = ChatBotSV["GLOBAL"]
    end
    local i = 1
    local reply = nil
    while list[i] do
        if not ChatBot.WasRecentlyUsed(specific,i,name) then
            local j = 1
            while list[i].msg[j] do
                if string.find(string.lower(message),string.lower(list[i].msg[j]),1,true) then
                    ChatBot.AddRecentlyUsed(specific,i,name)
                    if reply then
                        reply = reply.." "..ChatBot.SelectReply(list[i].reply)
                    else
                        reply = ChatBot.SelectReply(list[i].reply)
                    end
                    break
                end
                j=j+1
            end
        end
        i=i+1
    end
    if not reply then
        if list["DEFAULT"] then
            reply = ChatBot.SelectReply(list["DEFAULT"].reply)
        end
    end
    return reply
end

function ChatBot.SelectReply(list)
    local i = 1
    local priority = 0
    while list[i] do
        priority=priority+list[i].priority
        i=i+1
    end
    i = 1
    if priority == 0 then return nil end
    local rng = random(priority)
    while list[i] do
        if rng>list[i].priority then
            rng=rng-list[i].priority
        else
            return list[i].msg
        end
        i=i+1
    end
    return nil
end

local f = CreateFrame("Frame")
f:RegisterEvent("ADDON_LOADED")
f:RegisterEvent("CHAT_MSG_WHISPER")
f:SetScript("OnEvent", ChatBot.OnEvent)
